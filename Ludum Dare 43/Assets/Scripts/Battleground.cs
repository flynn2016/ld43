﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battleground : MonoBehaviour {

    public PlayerController playerController;

    public float enemy_army;
    public float your_army;
    private float time_speed;
    public bool battle_finished;
    public bool battle_started;

    private float start_enemy;
    private float start_you;
    private bool is_you_favored;
    private float dist_to_player;
    public bool isRetreating;

    private float unit_lose_favored = 0.05f;
    public float unit_speed = 0.002f;
    public GameObject enemyArmy;
    public GameObject yourArmy;

    public GameObject redFlag;
    public GameObject blueFlag;
    public GameObject BattleSign;

    public enum AttackMode {Heavy, Normal, Defend };
    public AttackMode attackmode;

    private void Start()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        start_enemy = enemy_army;
        start_you = your_army;
        attackmode = AttackMode.Normal;
        Calculate();
    }

    private void Update()
    {
        Calculate();
        //Debug.Log("your army: " + your_army+"enemy "+enemy_army);

        Distance();
    }

    private void Distance()
    {
        dist_to_player=Vector3.Distance(playerController.transform.position,this.transform.position);
        if (dist_to_player < 10)
            time_speed = unit_speed;
        else if (dist_to_player > 10 && dist_to_player < 30)
            time_speed = unit_speed / 3;
        else if (dist_to_player > 30)
            time_speed = unit_speed / 10;
    }

    private void Calculate()
    {
        is_you_favored = ((enemy_army < your_army) ? true : false);
        //favored_left = ((is_you_favored == true) ? Mathf.Sqrt(Mathf.Pow(start_you, 2) - Mathf.Pow(start_enemy, 2)) :
        //    Mathf.Sqrt(Mathf.Pow(start_enemy, 2) - Mathf.Pow(start_you, 2)));

        if (!battle_finished)
        {

            if (is_you_favored == true)
            {

                if (attackmode == AttackMode.Normal)
                {
                    your_army -= unit_lose_favored;
                    enemy_army -= unit_lose_favored * 2.5f;
                }

                else if (attackmode == AttackMode.Defend)
                {
                    your_army -= unit_lose_favored * 0.5f;
                    enemy_army -= unit_lose_favored;
                }

                else if (attackmode == AttackMode.Heavy)
                {
                    your_army -= unit_lose_favored * 3;
                    enemy_army -= unit_lose_favored * your_army / 100;
                }

            }
            else
            {
                if (attackmode == AttackMode.Normal)
                {
                    your_army -= unit_lose_favored;
                    enemy_army -= unit_lose_favored * 2.5f;
                }

                else if (attackmode == AttackMode.Defend)
                {
                    your_army -= unit_lose_favored;
                    enemy_army -= unit_lose_favored * 1.5f;
                }

                else if (attackmode == AttackMode.Heavy)
                {
                    your_army -= unit_lose_favored * 4;
                    enemy_army -= unit_lose_favored * 3;
                }
            }

            if ((enemy_army - 0.01f < 0 || your_army - 0.01f < 0||isRetreating)&&!battle_finished)
            {
                yourArmy.SetActive(false);
                enemyArmy.SetActive(false);
                if (is_you_favored == true&&!isRetreating)
                {
                    enemy_army = 0f;
                    blueFlag.SetActive(true);
                    playerController.city_retrieved++;
                    playerController.total_kills += (int)start_enemy;
                }
                else if (isRetreating)
                {
                    redFlag.SetActive(true);
                    playerController.city_lost++;
                    playerController.total_kills += (int)(start_enemy-enemy_army);

                }
                else 
                {
                    your_army = 0f;
                    redFlag.SetActive(true);
                    playerController.city_lost++;
                }
                battle_finished = true;
                BattleSign.SetActive(false);
            }

            if (isRetreating)
            {
                Debug.Log("here");
                your_army *= 0.75f;
            }

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!battle_finished)
        {
            battle_started = true;
            your_army = other.GetComponent<PlayerController>().army+your_army;
            Debug.Log(other.GetComponent<PlayerController>().army + " " + your_army);
            playerController.currentBattleground = this.GetComponent<Battleground>();
            unit_lose_favored *= 8;
        }

    }


}
