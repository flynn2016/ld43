﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed = 3f;
    public float army;
    public bool disableMovement;
    public float speed_accelration =0.01f;
    [HideInInspector]
    public float speed_coefficient = 0.5f;
    public float snow_slowspeed = 0.5f;
    public float foreset_slowspeed = 0.7f;
    public float river_slowspeed = 0.5f;
    public float cliff_slowspeed = 0.1f;
    public bool isPaused;
    public bool confirmed;
    public Transform currFlag;
    public bool hasDesitnation;
    public bool IsAmbushed;
    public int city_retrieved;
    public int city_lost;
    public int total_kills;

    public Image Death;
    public Image Frost;

    private float maxiumspeed;

    public AudioClip walkSound;
    public AudioSource walkSource;

    public Transform flag;
    private Vector3 target;
    public Camera cam;
    public Battleground currentBattleground;
    private Animator player_animation;
    private RaycastHit hit;
    public UIController uIController;

    public GameObject confirmPage;

    public enum Enviroment {Snow, River, Forest, Cliff, Normal};
    public Enviroment enviroment;


    // Use this for initialization
    void Start()
    {
        enviroment = Enviroment.Normal;
        player_animation = this.GetComponent<Animator>();
        target = transform.position;
        maxiumspeed = 2 * speed;

    }

    // Update is called once per frame
    void Update()
    {
        Move();
       
        Reach_destination();
        UpdateArmy();

        player_animation.SetFloat("move_speed",speed*speed_coefficient);

        //Playsound();

        }

    private void UpdateArmy()
    {

        if (speed_coefficient > 0.8 && hasDesitnation == true)
        {
            Death.enabled = true;
            army -= (((speed_coefficient - 0.8f) * 0.04f));
        }
        else
            Death.enabled = false;
    }

    public void Reach_destination()
    {
        if (Vector3.Distance(this.transform.position, target) <= 0f)
        {
            if (currFlag != null)
            {
                Destroy(currFlag.gameObject);
            }
            hasDesitnation = false;
            confirmed = false;
        }

    }



    public void Move()
    {

        if (Input.GetMouseButtonDown(1) && !disableMovement && !hasDesitnation&& !confirmed )
        {
            confirmPage.SetActive(true);
            target = cam.ScreenToWorldPoint(Input.mousePosition);
            currFlag = Instantiate(flag, new Vector3(target.x, target.y, 0), Quaternion.identity);
            target.z = transform.position.z;
            if (target.x > this.transform.position.x)
            {
                this.transform.localScale = new Vector3(-1,this.transform.localScale.y,this.transform.localScale.z);
            }
            else
            {
                this.transform.localScale = new Vector3(1, this.transform.localScale.y, this.transform.localScale.z);
            }
        }

        if (!disableMovement)
        {
            if (Input.GetKey(KeyCode.D))
            {
                if (speed_coefficient < 2)
                    speed_coefficient += speed_accelration;
                else
                    speed_coefficient = 2;
            }

            if (Input.GetKey(KeyCode.A))
            {
                if (speed_coefficient > 0)
                    speed_coefficient -= speed_accelration;

                else
                    speed_coefficient = 0;
            }
            if (speed_coefficient > 0.5) //normal linear update movement
            {

                if (confirmed&&!isPaused)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime * speed_coefficient);
                }

            }
            else if (speed_coefficient <= 0f) // reach zero speed
            {
                speed_coefficient = 0;
            }
            else //1/4 update movement
            {

                if (confirmed)
                {
                    if (confirmed && !isPaused)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, target, (maxiumspeed / 4) * Time.deltaTime); //update movement
                    }
                }
            }
        }

        if (currentBattleground != null)
        {

            if (currentBattleground.battle_finished != true)//during battle
            {
                disableMovement = true;
            }
            else //battle finished
            {
                disableMovement = false;
                target = transform.position;
                army = currentBattleground.your_army; 
                currentBattleground = null;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Snow_mountain")
        {
            Frost.enabled = true;
            if (!uIController.Trigger_frost)
            {
                isPaused = true;
            }
            speed_coefficient *= snow_slowspeed;
            enviroment = Enviroment.Snow;
        }

        else if(other.tag == "Forest")
        {

            IsAmbushed = (UnityEngine.Random.Range(1, 10) > 4);
            if (IsAmbushed)
            {
                army *= 0.8f;

            }

            if (!uIController.Trigger_Ambush&&IsAmbushed)
            {
                isPaused = true;
            }
            speed_coefficient *= foreset_slowspeed;
            enviroment = Enviroment.Forest;


        }

        else if (other.tag == "River")
        {
            if (!uIController.Trigger_Drown)
            {
                isPaused = true;
            }
            speed_coefficient *= river_slowspeed;
            enviroment = Enviroment.River;
            army *= (1 - UnityEngine.Random.Range(5, 20) * 0.01f);
        }
        else if (other.tag == "Cliff")
        {
            if (!uIController.Trigger_Cliff)
            {
                isPaused = true;
            }
            speed_coefficient *= cliff_slowspeed;
            enviroment = Enviroment.Cliff;
            army *= (1 - UnityEngine.Random.Range(15, 30) * 0.01f);
        }
    }



    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Snow_mountain")
        {
            Frost.enabled = false;
            speed_coefficient /= snow_slowspeed;
        }
        else if (other.tag == "Forest")
        {
            speed_coefficient /= foreset_slowspeed;
        }
        else if (other.tag == "River")
        {
            speed_coefficient /= river_slowspeed;

        }
        else if (other.tag == "Cliff")
        {
            speed_coefficient /= cliff_slowspeed;
        }
        enviroment = Enviroment.Normal;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Snow_mountain")
        {
           
            army -= 0.04f;
        }
        else
            Frost.enabled = false;
    }
}

