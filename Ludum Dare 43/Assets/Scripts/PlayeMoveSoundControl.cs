﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayeMoveSoundControl : MonoBehaviour {

    // Use this for initialization

    public GameObject player;
    private bool isMoving = false;
    private bool isPlaying = false;

    private AudioSource audiosource;
    private Transform tr;

    private Vector3 lastFramPos;
    private Vector3 currentFramPos;


    void Start () {
        audiosource = player.GetComponent<AudioSource>();
        tr = player.transform;
        lastFramPos = tr.position;
	}
	

	void Update () {

        currentFramPos = tr.position;
        if (currentFramPos != lastFramPos)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
            audiosource.Stop();
            isPlaying = false;
        }
        if (isMoving)
        {
            if (isPlaying == false)
            {
                isPlaying = true;
                audiosource.Play();
            }
        }
        lastFramPos = currentFramPos;

	}
}
