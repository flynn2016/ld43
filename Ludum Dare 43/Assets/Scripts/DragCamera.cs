﻿using UnityEngine;
using System.Collections;

public class DragCamera : MonoBehaviour
{
    public float dragSpeed = 2;
    private Vector3 dragOrigin;
    private Vector3 move;
    private Vector3 pos;

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {

            dragOrigin = Input.mousePosition;
            pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        }

        if (Input.GetMouseButton(0))
        {
            pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
            move = Vector3.Lerp(move, new Vector3(-pos.x * dragSpeed, -pos.y * dragSpeed, 0), 2f);
            transform.Translate(move, Space.World);
        }

        if (Camera.main.transform.position.x > 2.5f)
        {
            Camera.main.transform.position = new Vector3(2.49f, Camera.main.transform.position.y, Camera.main.transform.position.z);
        }

        if (Camera.main.transform.position.x < -2.5f)
        {
            Camera.main.transform.position = new Vector3(-2.49f, Camera.main.transform.position.y, Camera.main.transform.position.z);
        }
        if (Camera.main.transform.position.y < -7f)
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, -6.99f, Camera.main.transform.position.z);
        }

        if (Camera.main.transform.position.y > 7)
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, 6.99f, Camera.main.transform.position.z);
        }

    }


}