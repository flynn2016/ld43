﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberDigitsController : MonoBehaviour
{

    private string rawNumber;
    private float number;
    private float timer;

    private SpriteRenderer[] digitsList = new SpriteRenderer[4];
    private int[] digits = new int[4];
    //public Sprite[] numberSprites = new Sprite[10];

    public Sprite[] numberSprite = new Sprite[10];

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 1f)
        {
            Calculate();
            timer = 0;
        }
    }

    private void Calculate()
    {

        if (this.name == "Your Army")
        {
            number = GetComponentInParent<Battleground>().your_army;
        }
        else if (this.name == "Enemy Army")
        {
            number = GetComponentInParent<Battleground>().enemy_army;
        }

        for (int i = 0; i < 4; i++)
        {
            digits[i] = 0;
        }

        digitsList[0] = this.transform.GetChild(3).GetComponent<SpriteRenderer>();
        digitsList[1] = this.transform.GetChild(2).GetComponent<SpriteRenderer>();
        digitsList[2] = this.transform.GetChild(1).GetComponent<SpriteRenderer>();
        digitsList[3] = this.transform.GetChild(0).GetComponent<SpriteRenderer>();

        if (number < 1000)
        {
            digitsList[3].enabled = false;
        }
        if (number < 100)
        {
            digitsList[2].enabled = false;
        }
        if (number < 10)
        {
            digitsList[1].enabled = false;
        }

        ConvertDigits(number);
    }

    private void ConvertDigits(float n)
    {
        for (int i = 0; i < 4; i++)
        {
            digits[i] = (int)n % 10;
            //Debug.Log("the digit is: " + digits[i]);
            digitsList[i].sprite = numberSprite[digits[i]];
            n /= 10;
        }
    }
}