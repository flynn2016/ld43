﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    private PlayerController playerController;
    public Text army_display;
    public RectTransform arrow;
    private float temp;
    public Text Enemy;
    public Text Your_army;
    public GameObject BattleUI;

    public Image HeavyAttack;
    public Image NormalAttack;
    public Image DefendAttack;

    public GameObject Sword;
    public GameObject Shield;
    public GameObject Sword_shield;
    public GameObject confirmPage;


    public Image Snow;
    public Image Forest;
    public Image River;

    public GameObject Ambush;
    public GameObject Frost;
    public GameObject Cliff;
    public GameObject Drown;

    public GameObject startScene;
    public GameObject introduction;
    public GameObject endScene;

    [HideInInspector]
    public bool finish_event;
    public bool Trigger_frost;
    public bool Trigger_Ambush;
    public bool Trigger_Cliff;
    public bool Trigger_Drown;


    public Text City_retrieved;
    public Text City_lost;
    public Text Total_kills;

    public Text end_City_retrieved;
    public Text end_City_lost;
    public Text end_Total_kills;

    public GameObject tutorial;

    private float timer=0;


    // Use this for initialization
    void Start () {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (playerController.currentBattleground != null)
        {
            army_display.text = "" + (int)playerController.currentBattleground.your_army;
        }
        else
            army_display.text = "" + (int)playerController.army;

        UpdateArrow();
        UpdateBattle();
        UpdateEnviroment();
        UpdateScore();

    }


    public void SwitchIntroScene()
    {
        startScene.SetActive(false);
        introduction.SetActive(true);

    }

    public void SwitchGameScene()
    {
        introduction.SetActive(false);
    }

    private void UpdateScore()
    {
        if (playerController.army < 1f||playerController.city_lost==5||playerController.city_retrieved==5||(playerController.city_lost+playerController.city_retrieved)==5){
            endScene.SetActive(true);
        }
        City_retrieved.text = ""+playerController.city_retrieved;
        City_lost.text = "" + playerController.city_lost;
        Total_kills.text = ""+playerController.total_kills;

        end_City_retrieved.text = "" + playerController.city_retrieved;
        end_City_lost.text = "" + playerController.city_lost;
        end_Total_kills.text = "" + playerController.total_kills;
    }

    private void UpdateEnviroment()
    {
        if (playerController.enviroment == PlayerController.Enviroment.Snow)
        {
            River.enabled = false;
            Forest.enabled = false;
            Snow.enabled = true;

            if (!finish_event&&!Trigger_frost)
            {
                Frost.SetActive(true);
                Cliff.SetActive(false);
                Drown.SetActive(false);
                Ambush.SetActive(false);
                Trigger_frost = true;
            }
        }

        else if (playerController.enviroment == PlayerController.Enviroment.River)
        {
            River.enabled = true;
            Forest.enabled = false;
            Snow.enabled = false;

            if (!finish_event&&!Trigger_Drown)
            {
                Frost.SetActive(false);
                Cliff.SetActive(false);
                Drown.SetActive(true);
                Ambush.SetActive(false);
                Trigger_Drown = true;
            }
        }

        else if (playerController.enviroment == PlayerController.Enviroment.Forest)
        {
            River.enabled = false;
            Forest.enabled = true;
            Snow.enabled = false;

            if (!finish_event&&!Trigger_Ambush)
            {
                if (playerController.IsAmbushed)
                {
                    Frost.SetActive(false);
                    Cliff.SetActive(false);
                    Drown.SetActive(false);
                    Ambush.SetActive(true);
                    Trigger_Ambush = true;
                }
                else
                {
                    Frost.SetActive(false);
                    Cliff.SetActive(false);
                    Drown.SetActive(false);
                    Ambush.SetActive(false);
                    Trigger_Ambush = false;
                }

            }
        }

        else if (playerController.enviroment == PlayerController.Enviroment.Cliff)
        {
            River.enabled = false;
            Forest.enabled = false;
            Snow.enabled = false;

            if (!finish_event&&!Trigger_Cliff)
            {
                Frost.SetActive(false);
                Cliff.SetActive(true);
                Drown.SetActive(false);
                Ambush.SetActive(false);
                Trigger_Cliff = true;
            }
        }

        else
        {

            River.enabled = false;
            Forest.enabled = false;
            Snow.enabled = false;

            Frost.SetActive(false);
            Cliff.SetActive(false);
            Drown.SetActive(false);
            Ambush.SetActive(false);
            finish_event = false;
        }
    }

    private void UpdateArrow()
    {
        temp = Mathf.Lerp(temp,200 * (playerController.speed_coefficient - 1), 0.1f);
        arrow.anchoredPosition = new Vector2(temp, arrow.anchoredPosition.y);
    }

    private void UpdateBattle()
    {
        timer += Time.deltaTime*2f;

        if (playerController.currentBattleground != null)
        {
            if (playerController.currentBattleground.battle_started == true)
            {
                BattleUI.SetActive(true);
            }

            if (timer > 1f)
            {

                Enemy.text = "Enemy: " + (int)(playerController.currentBattleground.enemy_army);
                Your_army.text = "Your Army: " + (int)(playerController.currentBattleground.your_army);
                timer = 0;
            }

        }
        else
            BattleUI.SetActive(false);
    }

    public void Heavybutton()
    {
        HeavyAttack.color = new Color(0.5f,0.5f,0.5f,1f);
        NormalAttack.color = new Color(1,1,1,1);
        DefendAttack.color = new Color(1,1,1,1);
        playerController.currentBattleground.attackmode = Battleground.AttackMode.Heavy;
        Sword.SetActive(true);
        Shield.SetActive(false);
        Sword_shield.SetActive(false);
    }

    public void Normalbutton()
    {
        HeavyAttack.color = new Color(1, 1, 1, 1);
        NormalAttack.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        DefendAttack.color = new Color(1, 1, 1, 1);
        playerController.currentBattleground.attackmode = Battleground.AttackMode.Normal;
        Sword.SetActive(false);
        Shield.SetActive(false);
        Sword_shield.SetActive(true);
    }

    public void Defendbutton()
    {
        HeavyAttack.color = new Color(1, 1, 1, 1);
        NormalAttack.color = new Color(1, 1, 1, 1);
        DefendAttack.color = new Color(0.5f, 0.5f, 0.5f, 1f);
        playerController.currentBattleground.attackmode = Battleground.AttackMode.Defend;
        Sword.SetActive(false);
        Shield.SetActive(true);
        Sword_shield.SetActive(false);

    }

    public void Retreat()
    {
        playerController.currentBattleground.isRetreating = true;
    }

    public void ConfirmYes()
    {
        confirmPage.SetActive(false);
        playerController.confirmed = true;
        playerController.hasDesitnation = true;

    }

    public void ConfirmNo()
    {
        confirmPage.SetActive(false);
        playerController.confirmed = false;
        playerController.hasDesitnation = false;
        Destroy(playerController.currFlag.gameObject);

    }

    public void Unpause()
    {
        playerController.isPaused = false;
        Ambush.SetActive(false);
        Frost.SetActive(false);
        Drown.SetActive(false);
        Cliff.SetActive(false);
        finish_event = true;

    }

    public void reload()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void Close()
    {
        tutorial.SetActive(false);
    }
}
